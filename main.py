#!/usr/bin/env python3

import sys
try:
    import yaml
    import json
    from multiprocessing import Process, Queue
    import getopt
    import jsonschema
    import os
    import datetime
    import time
    import subprocess
    import string
    import re
    import shutil
    from ansible.module_utils.common.collections import ImmutableDict
    from ansible.parsing.dataloader import DataLoader
    from ansible.vars.manager import VariableManager
    from ansible.inventory.manager import InventoryManager
    from ansible import context
    from ansible.cli import CLI
    from ansible.executor.playbook_executor import PlaybookExecutor
    from configparser import ConfigParser

except ModuleNotFoundError as ex:
    print(ex)
    sys.exit(1)


def runCMD(cmd, hideLogs=None, read=None):
    """ Run the command.

    Parameters:
        cmd (str): Command to be run
        hideLogs (bool): Indicates whether cmd logs should be hidden
        read (bool): Indicates whether logs of the command should be returned

    Returns:
        int or str: Command's exit code. Logs of the command if read=True
    """

    if read is True:
        return os.popen(cmd).read().strip()
    if hideLogs is True:
        return subprocess.call(
            cmd, shell=True, stdout=open(
                os.devnull, 'w'), stderr=subprocess.STDOUT)
    else:
        return os.system(cmd)


def loadFile(loadThis, required=None):
    """ Loads a file

    Parameters:
        loadThis (str): Path to the file to load
        required (bool): Indicates whether the file is required

    Returns:
        str or yaml: The loaded file or empty string if the file was not found
                     and required is not True. Exits with code 1 otherwise.
    """

    if os.path.exists(loadThis) is False:
        if required is True:
            print(loadThis + " file not found")
            sys.exit(1)
        else:
            return ""
    with open(loadThis, 'r') as inputfile:
        if ".yaml" in loadThis:
            try:
                return yaml.load(inputfile, Loader=yaml.FullLoader)
            except AttributeError:
                try:
                    return yaml.load(inputfile)
                except:  # yaml.scanner.ScannerError:
                    print("Error loading yaml file " + loadThis)
                    sys.exit(1)
            except:  # yaml.scanner.ScannerError:
                print("Error loading yaml file " + loadThis)
                sys.exit(1)
        else:
            return inputfile.read().strip()


def getIP():
    """Get IP from terraform"""
    return runCMD("terraform show -json | jq .values.root_module.resources[0].values.public_ip", read=True).replace('"', '')


def runRemote(file):
    runCMD("ssh -i %s -o StrictHostKeyChecking=no root@%s 'bash -s' < %s" % (configs["sshKey"], getIP(), file))


def validateConfigs(configs):
    """ Validates configs.yaml against schemas.

    Parameters:
        configs (dict): Object containing configs.yaml's configurations.
    """

    try:
        jsonschema.validate(configs, loadFile("configs_sch.yaml"))
    except jsonschema.exceptions.ValidationError as ex:
        print("Error validating configs file: \n %s" % ex.message)
        sys.exit(1)


def ansiblePlaybook(configs, playbookPath, user=None):
    """Runs ansible-playbook with the given playbook.

    Parameters:
        configs (dict): Content of configs.yaml.
        configs (dict): Content of configs.yaml.
        configs (dict): Content of configs.yaml.

    Returns:
        int: 0 for success, 1 for failure
    """

    hostsFilePath = "hosts"

    if user is None:
        user = "root"

    loader = DataLoader()

    context.CLIARGS = ImmutableDict(
        tags={},
        private_key_file=configs["sshKey"],
        connection='ssh',
        remote_user=user,
        become_method='sudo',
        ssh_common_args='-o StrictHostKeyChecking=no',
        forks=100,
        verbosity=True,
        listtags=False,
        listtasks=False,
        listhosts=False,
        syntax=False,
        check=False,
        start_at_task=None
    )

    inventory = InventoryManager(loader=loader, sources=hostsFilePath)
    variable_manager = VariableManager(loader=loader,
                                       inventory=inventory,
                                       version_info=CLI.version_info(
                                           gitinfo=False))

    return PlaybookExecutor(playbooks=[playbookPath],
                           inventory=inventory,
                           variable_manager=variable_manager,
                           loader=loader,
                           passwords=None).run()


def createHostsFile(ip,destination):
    """Creates the hosts file required by ansible. Note destination contains
       the string 'hosts' too.

    Parameters:
        ip (str): IP address of the machine.
        destination (str): Destination of hosts file.

    Returns:
        int: 0 for success, 1 for failure
    """

    config = ConfigParser(allow_no_value=True)
    config.add_section('master')
    config.set('master', "%s ansible_python_interpreter=/usr/bin/python3" % ip) # TODO: is this ansible_python_interpreter really needed?
    with open(destination, 'w') as hostsfile:
        config.write(hostsfile)


def makeCredFile(): # TODO: this would ease the config but it can be a risk
    """Take access keys from configs.yaml and generates the shared
       credentials file."""

    scfPath = "~/.awsCreds"


cfgPath = None
skipAnsible = False
runExample = False
runJupyter = False
destroy = False
skipAnsible = False
skipTerraform = False


# -----------------CMD OPTIONS--------------------------------------------
try:
    options, values = getopt.getopt(
        sys.argv[1:],
        "c:",
        ["skip-ansible",
         "skip-vm-configuration",
         "skip-terraform",
         "skip-vm-creation",
         "run-example",
         "run-jupyter",
         "destroy"])
except getopt.GetoptError as err:
    print(err)
    sys.exit(1)
for currentOption, currentValue in options:
    if currentOption in ['-c']:
        cfgPath = currentValue
    elif currentOption in ['--skip-ansible','--skip-vm-configuration']:
        skipAnsible = True
    elif currentOption in ['--skip-terraform','--skip-vm-creation']:
        skipTerraform = True
    elif currentOption in ['--run-example']:
        runExample = True
    elif currentOption in ['--run-jupyter']:
        runJupyter = True
    elif currentOption in ['--destroy']:
        destroy = True


if cfgPath is None:
    cfgPath = "configs.yaml"

configs = loadFile(cfgPath, required=True)
validateConfigs(configs)

# ~~~~~~~~~~~~~~~ CHECK OPTIONS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
#if runExample is True and onlyPrepare is True:
#    print("Options --run-example and --only-prepare can't be used at the same time")
#    sys.exit(1)
if skipTerraform is True and os.path.isfile("terraform.tfstate") is False:
    print("The VM was not created yet, running with --skip-terraform not possible.")
    sys.exit(1)

# ~~~~~~~~~~~~~~~ CHECK KEY PERMS. ARE 600 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ # 
if "600" not in oct(os.stat(configs["sshKey"]).st_mode & 0o777) and \
   "400" not in oct(os.stat(configs["sshKey"]).st_mode & 0o777):
    print("Key permissions must be set to 600 or 400")
    sys.exit(1)


def provision():
    with open("raw_vm", 'r') as infile:
        vm = infile.read().replace(
            "REGION_PH", configs['region']).replace(
            "SSH_KEY_PATH_PH", configs['sshKey']).replace(
            "KEY_NAME_PH", configs['keyName']).replace(
            "AMI_PH", configs['ami']).replace(
            "OPEN_USER_PH", configs['openUser']).replace(
            "VM_TYPE_PH", configs['vmType']).replace(
            "VS_PH", str(configs['volumeSize']))

    with open ("vm.tf", 'w') as outfile:
        outfile.write(vm)
    # ---------------- RUN TERRAFORM: provision VMs
    return runCMD("terraform init && terraform apply -auto-approve")

if skipTerraform is not True:
    if provision() != 0:
        print("ERROR: failed to provision machine.")
        sys.exit(1)

# ~~~~~~~~~~~~~~~ PREPARE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
if skipAnsible is not True:
    createHostsFile(getIP(), "hosts")
    if ansiblePlaybook(configs, "playbooks/playbook.yaml", user=configs["openUser"]) != 0:
        print("ERROR: failed to prepare machine.")
        sys.exit(1)

# ~~~~~~~~~~~~~~~ RUN EXAMPLE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
if runExample is True:
    if ansiblePlaybook(configs, "playbooks/example.yaml") != 0:
        print("ERROR: example run failed.")
        sys.exit(1)
    runCMD("mkdir fetchedImgs &> /dev/null") # TODO: uncomment this
    runCMD("scp -o StrictHostKeyChecking=no -i %s root@%s:~/steps/img* fetchedImgs" % (configs["sshKey"], getIP()))

# ~~~~~~~~~~~~~~~ RUN JUPYTER ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
if runJupyter is True:
    if ansiblePlaybook(configs, "playbooks/jupyter.yaml") != 0:
        print("ERROR: failed to start jupyter notebook.")
        sys.exit(1)
    print("-----------------------------------------------------------------------------------")
    print("jupyter notebook launched (logs on the VM at /root/jupyterLogs).")
    print("To be able to access the Jupyter notebook on your machine run:")
    print("ssh -i %s -N -f -L 8888:localhost:8888 root@%s" % (configs["sshKey"], getIP()))
    print("-----------------------------------------------------------------------------------")
