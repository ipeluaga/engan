FROM ubuntu:18.04

# ------------------ Fix UnicodeEncodeError: 'export PYTHONIOENCODING=utf8'
ARG PYTHONIOENCODING=utf8

# ------------------ General Stuff
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y \
    curl \
    wget \
    awscli \
    python \
    python-pip \
    python3-pip \
    nano \
    vim \
    python2.7 \
    zip \
    unzip \
    git \
    snapd \
    net-tools \
    jq

RUN pip3 install \
    pyyaml \
    kubernetes \
    jsonschema \
    ansible

RUN pip3 install --upgrade requests

# ------------------ Install terraform
RUN TERRAFORM_VERSION=$(curl -s https://checkpoint-api.hashicorp.com/v1/check/terraform | jq -r .current_version) && \
    wget https://releases.hashicorp.com/terraform/$TERRAFORM_VERSION/terraform_${TERRAFORM_VERSION}_linux_amd64.zip && \
    unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip && \
    mv terraform /bin

# ------------------ Create ssh key file
RUN mkdir /root/.ssh && \
    touch /root/.ssh/id_rsa && \
    chmod 600 /root/.ssh/id_rsa

# ------------------ Create AWS credentials file
RUN mkdir /root/.aws
RUN echo "[default] \naws_access_key_id = \naws_secret_access_key =" > /root/.aws/credentials

# ------------------ Clone enGAN repo
ENTRYPOINT git clone -q https://gitlab.cern.ch/ipeluaga/engan.git && cd engan ; bash
