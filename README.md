# enGAN

## Configure and run

Linux with jq and unzip is required.
Refer to the section "Run with Docker" below to use the provided Docker image.

1) Create ~/.aws/credentials following the code below (These keys can be found at https://console.aws.amazon.com/iam/home#/security_credentials under "Access keys"):

```
[default]
aws_access_key_id = 12345ABCD
aws_secret_access_key = 12345ABCD
```

2) Clone this repo:

```bash
git clone https://gitlab.cern.ch/ipeluaga/engan.git
```

3) Edit configs.yaml to provide your configuration:

**Variable** | **Explanation**
 --- | ---
region  | AWS region on which the VM must be created
sshKey  | Path to your ssh key. It's permissions must be set to 600 or 400.
keyName  | Name of your ssh key on AWS
ami  | AWS AMI to be used
openUser  | User that can connect by default to the VM
vmType  | Machine type
volumeSize  | Volume size

Note that key has to be uploaded to AWS beforehand (at https://console.aws.amazon.com/ec2/v2/home#KeyPairs).

4) Download the latest version of terraform (https://www.terraform.io/downloads.html) and place the unzipped bin at /bin:

```bash
TERRAFORM_LATEST=$(curl -s https://checkpoint-api.hashicorp.com/v1/check/terraform | jq -r .current_version)
wget https://releases.hashicorp.com/terraform/$TERRAFORM_LATEST/terraform_${TERRAFORM_LATEST}_linux_amd64.zip
unzip terraform_${TERRAFORM_LATEST}_linux_amd64.zip
mv terraform /bin
```

5) Install Ansible with pip following the steps at https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-ansible-with-pip.

6) Run:

```bash
./main.py <options>
```

## Run with Docker

1) Install and start Docker (avoid this step if Docker is already installed). Alternatively, when running through a bastion, you can use for it the "Amazon ECS-Optimized Amazon Linux AMI" which has Docker already installed.

2) Pull the latest image and run it

```bash
docker pull cernefp/engan:latest && docker run -it cernefp/engan:latest
```

3) Inside the container, edit ~/.aws/credentials and ~/.ssh/id_rsa with your aws credentials and private key. Note that key has to be uploaded to AWS beforehand (at https://console.aws.amazon.com/ec2/v2/home#KeyPairs).

4) Inside the container, edit configs.yaml to provide your configuration:

**Variable** | **Explanation**
 --- | ---
region  | AWS region on which the VM must be created
sshKey  | Path to your ssh key (when using the Docker container this is '/root/.ssh/id_rsa')
keyName  | Name of your ssh key on AWS
ami  | AWS AMI to be used
openUser  | User that can connect by default to the VM
vmType  | Machine type
volumeSize  | Volume size

5) Run:

```bash
./main.py <options>
```

## Options

**Option** | **Explanation**
 --- | ---
--run-example  | Run the fake celebrities example
--skip-terraform or --skip-vm-creation  | Skip the terraform provisioning.
--skip-ansible or --skip-vm-configuration  | Skip the ansible bootstrap.
--run-jupyter  | Starts the Jupyter Notebook on the VM.
